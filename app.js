var express = require('express');
var fs = require('fs');
var https = require('https');
var path = require('path');
var Recaptcha = require('recaptcha').Recaptcha;
var app = express();
var api = require('./dbAPIs');
var uuid = require('uuid');
api.createDatabase();
var user = require('./models/user');
var enDeApi = require('./EN-DE-API');

app.set('views', path.join(__dirname, './', 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
app.use(express.session());
app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'https://localhost:5000');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});
app.use('/users/signup',function(req,res,next){

  var cookie = req.cookies.userId;
  if(cookie !== undefined){
    console.log('User already submitted the record from here.');
    res.json(409,{success:false, message:'You have already submitted the form once using this machine.'});
  }
  else{
    next();
  }
});

app.use(app.router);
app.use(express.static(path.join(__dirname,'./', 'CLIENT')));
app.use(express.static(path.join(__dirname, './', 'public')));


app.set('port', 5000);
var key = fs.readFileSync(path.join(__dirname,'./SSL/safariFone_key.pem'));
var cert = fs.readFileSync(path.join(__dirname,'./SSL/safariFone_cert.pem'));
var https_options = {
  key: key,
  cert: cert
};

app.post('/users/signup',function(req,res){
  var thisReq = req;
  var data = req.body.encryptedData;
  enDeApi.tripleSecDataDecrypt(data,enDeApi.tripleSecKeyDecrypt(req.body.key),function(originalData){
    api.createUser(originalData,function(code,data){
      if(code === 200){
        res.cookie('userId', uuid.v4());
        console.log('Cookies have been created for this user.');
        res.json(data);
      }
      else{
        res.json(data);
      }
    });

  });


});
app.post('/users/login',function(req,res){
  var data = req.body.encryptedData;
  console.log(data);
  enDeApi.tripleSecDataDecrypt(data,enDeApi.tripleSecKeyDecrypt(req.body.key),function(originalData){
    console.log(originalData);
    api.findUser(originalData,function(code,data){
      if(code === 200){
        res.json(data);
      }
      else{
        res.json(code,data);
      }
    })
  })

});
app.get('/drop',api.drop);

app.get('/ws',function(req, res){
  res.render('ws/wsindex.ejs');
});

app.get('/',function(req,res){
  res.render('index');
});

app.get('/check',function(req,res){
  res.send('Its HTTPs');
});

app.get('/activate/user/:username',function(req,res){
  var username = req.params.username;
  api.activateUser(username,function(response){
    res.json(response);
  })
});



var server = https.createServer(https_options, app).listen(process.env.PORT || app.get('port'), function(){
  console.log('HTTPS Express server listening on port ' + (process.env.PORT || app.get('port')));
});

var socket = require('socket.io');
var io = socket.listen(server);

io.sockets.on('connection', function (socket) {
  io.sockets.connected['nickname'] ={ socketName: socket.id };
  socket.on('getSignUpData',function(data){

    if(io.sockets.connected['nickname'] && !('signedUp' in io.sockets.connected['nickname']) ){
      data.activated = false;
      console.log(data);
      enDeApi.tripleSecDataDecrypt(data.encryptedData,enDeApi.tripleSecKeyDecrypt(data.key),function(originalData){
        console.log(originalData);
        api.createUser(originalData,function(code,response){
          if(code === 200){
            io.sockets.connected['nickname']['signedUp'] = true;
            io.to(io.sockets.connected['nickname']['socketName']).emit('signUpConfirm',response);
          }
          else{
            io.to(io.sockets.connected['nickname']['socketName']).emit('signUpConfirm',response);
          }
        });
      })
    }
    else{
      io.to(io.sockets.connected['nickname']['socketName']).emit('signUpConfirm',{ success:false, message:'You have already submitted the form once using this machine.'});
    }

  });

  socket.on('getLoginData',function(data){
    console.log(data);
    enDeApi.tripleSecDataDecrypt(data.encryptedData,enDeApi.tripleSecKeyDecrypt(data.key),function(originalData){
      console.log(originalData);
      api.findUser(originalData,function(code,response){
        if(code === 200){
          io.to(io.sockets.connected['nickname']['socketName']).emit('loginConfirm',response);
        }
        else{
          io.to(io.sockets.connected['nickname']['socketName']).emit('loginConfirm',response);
        }
      });
    })
  });

  io.to(io.sockets.connected['nickname']['socketName']).emit('status', { status: 'Connected'}); // note the use of io.sockets to emit but socket.on to listen
});
