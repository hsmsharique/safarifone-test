/**
 * Created by hsmsharique on 9/1/14.
 */

var NodeRsa = require('node-rsa');
var privateKey = require('../KEYS/privateKeyProvider').privateKey;
var tripleSec = require('triplesec');
var nodeKey = new NodeRsa(privateKey);

module.exports = {

  tripleSecKeyDecrypt:function(encryptedKey){
    var decryptedTripleSecKey = nodeKey.decrypt(encryptedKey,'utf8');
    return decryptedTripleSecKey;
  },

  tripleSecDataDecrypt:function(enData,key,callBack){
    var decryptedData = tripleSec.decrypt({
      data:          new tripleSec.Buffer(enData, "hex"),
      key:           new tripleSec.Buffer(key)
    }, function (err, buff) {
      if (!err) {
        console.log(buff.toString());
        callBack(JSON.parse(buff.toString()));
      }
    })
  }
};
