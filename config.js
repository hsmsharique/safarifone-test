/**
 * Created by hsmsharique on 8/28/14.
 */


module.exports = {

  /*Email Service to be used with nodemailer module*/
  emailService      :'Gmail',

  /*Email address which will be used to send links to the user provided emails*/
  emailUser         :'noreply.safarifone@gmail.com',

  /*Password for the Email User used for nodemailer module*/
  emailUserPass     :'safarifone12345',

  /*MYSQL db host address*/
  dbHost            :'localhost',

  /*MYSQL username. I set it as of mine. Change it according to yours*/
  sqlUser           :'root',

  /*MYSQL password. I set it as of mine. Change it according to yours*/
  sqlPassword       :'root',

  /*DB name to be used in MYSQL*/
  dbName            :'safarifone'

};
