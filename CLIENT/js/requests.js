$(function() {

  var socket = io.connect('https://' + window.location.host);
  var crypt = new JSEncrypt();
  crypt.setPublicKey(utils.PUBLIC_Key);

  socket.on("status",function(data){
    console.log(data);
  });

  window.toastr.options.positionClass = 'toast-top-full-width';
  window.toastr.options.closeButton = true;

  var signUp = document.getElementById('signUpSelect');
  var login = document.getElementById('loginSelect');

  var baseHTTPSURL = "https://" + window.location.host + "/users";


  $("#register").click(function(){
    if(!checkValidEmail($("#username").val())){
      $("#username").parent().addClass("has-error");
      return false;
    }
    channelCheck(signUp);
    if(signUp.options[signUp.selectedIndex].value === 'https'){
      var enData = {
        firstname:document.getElementById('firstname').value,
        lastname:document.getElementById('lastname').value,
        password:document.getElementById('password').value,
        username:document.getElementById('username').value
      }
      enData = JSON.stringify(enData);
      var key = uuid.v4();
      triplesec.encrypt({
        data: new triplesec.Buffer(enData),
        key : new triplesec.Buffer(key)
      },function(err,buff){
        if(!err){
          $.ajax({
            url: baseHTTPSURL+'/signup',
            type:'POST',
            crossDomain:true,
            data: {
              encryptedData   : buff.toString('hex'),
              key             : crypt.encrypt(key)
            },
            success:function(data){
              window.toastr.success(data.message,'Sign-Up');
            },
            error:function(data){
              window.toastr.error(JSON.parse(data.responseText).message,'Sign-Up');
            }
          });
        }
      });
    }
    if(signUp.options[signUp.selectedIndex].value === 'wss'){
      var enData = {
        firstname:document.getElementById('firstname').value,
        lastname:document.getElementById('lastname').value,
        password:document.getElementById('password').value,
        username:document.getElementById('username').value
      }
      enData = JSON.stringify(enData);
      var key = uuid.v4();
      triplesec.encrypt({
        data  : new triplesec.Buffer(enData),
        key   : new triplesec.Buffer(key)
      },function(err,buff){
        if(!err){
          socket.emit('getSignUpData',{
            encryptedData   : buff.toString('hex'),
            key             : crypt.encrypt(key)
          });
        }
      });
      socket.on('signUpConfirm',function(data){
        if(data.success){
          window.toastr.success(data.message,'Sign-Up');
        }
        else{
          window.toastr.error(data.message,'Sign-Up');
        }
      });
    }
});

  $("#doLogin").click(function(){
    if(!checkValidEmail($("#username_login").val())){
      $("#username_login").parent().addClass("has-error");
      return false;
    }

    channelCheck(login);
    if(login.options[login.selectedIndex].value === 'https'){
      var enData = {
        username:document.getElementById('username_login').value,
        password:document.getElementById('password_login').value
      }
      enData = JSON.stringify(enData);
      var key = uuid.v4();
      triplesec.encrypt({
        data    : new triplesec.Buffer(enData),
        key     : new triplesec.Buffer(key)
      },function(err,buff){
        if(!err){
          $.ajax({
            url:baseHTTPSURL+'/login',
            type:'POST',
            crossDomain:true,
            data:{
              encryptedData   : buff.toString('hex'),
              key             : crypt.encrypt(key)
            },
            success:function(response){
              window.toastr.success("User successfully logged-In",'Login');
              console.log(response);
            },
            error:function(response) {
              window.toastr.error(JSON.parse(response.responseText).message,'Login');
              console.log(JSON.parse(response.responseText));
            }
          });
        }
      })
    }

    if(login.options[login.selectedIndex].value === 'wss'){
      var enData = {
        username:document.getElementById('username_login').value,
        password:document.getElementById('password_login').value
      }
      enData = JSON.stringify(enData);
      var key = uuid.v4();
      triplesec.encrypt({
        data    : new triplesec.Buffer(enData),
        key     : new triplesec.Buffer(key)
      },function(err,buff){
        if(!err){
          socket.emit('getLoginData',{
            encryptedData   : buff.toString('hex'),
            key             : crypt.encrypt(key)
          });
        }
      });
      socket.on('loginConfirm',function(data){
        if(data.success){
            window.toastr.success("User successfully logged-In",'Login');
        }
        else{
          window.toastr.error(data.message,'Login');
        }
      });
    }
  });


  function channelCheck(modal){
    if(modal.options[modal.selectedIndex].value === 'noChannel'){
      $("#"+modal.id+"Div").addClass('has-error');
      return;
    }
  }
  function checkValidEmail(email){
    var emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;

    if(email.match(emailRegex)){
      return true;
    }
  }
//
});
