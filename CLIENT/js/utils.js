(function(window){
  window.utils = {} || window.utils

  var publicKey = "-----BEGIN PUBLIC KEY-----"+
"MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAreIq+GOSBBu/n/mwYk/3"+
"opHa1u0usjDgs0V3GCGPcOFEiXmS6V+yPtsbwoYqBKgDmR8ZDGzbYWzrEUDtJBxg"+
"KLAKjOvfhXjYY4t8RLJ4mk+ipsh48WvF85mfvIhsTW5JwGy0byUSCFk4dPkvgHJC"+
"+Z5VqTT7RtX55p1cF751o0PbVtosdzFmHhrxk+9c3eYcRmoQRAtS/t2U6lon/Vi3"+
"DH8A3o/bKXXBsSf3r7xojMnnkb2xfwP0xOllC6nuyveynLbOPjRZa0gmUPhi14lY"+
"VWDn2r5thybU1SCU5Dq290OHZDZUjg1+BWDR8WxyW6Y5EQR3XQ3NzwUKRwAxEe59"+
"Suonr8FftZJHj0KVIbSSWJ08oGacJtmmImF/6OhtZu8ZcfI8zmLbefW+kX0PpOB4"+
"x0dhvsLtJ38Qwy4AOXtEVZcRWiA6MM/8may9s51ck3Y3rQcoR0yWu+UwOUjXSfBT"+
"PEcUT2HgeVL1iqW1n5wiIydQntbRcGOFCg9RD+gTFzYCtT6fiJUrnjQfS8m1FA+v"+
"9laKQQS86WvnaJ9x8gpehpXDAzXdZw5khFyvjNzdZJbmUFdVI1+1wR0E2u1pHJGu"+
"hmcKWbXSwJ4D9zfR0Aq9pkJAOt4q28d6+EU78gYfB7KJd6XDMtVJcUmiVu0HhpkQ"+
"kbP+Wt9cM5lI7mQi8rWBz98CAwEAAQ=="+
"-----END PUBLIC KEY-----";

window.utils.PUBLIC_Key = publicKey;



})(window)
