/**
 * Created by hsmsharique on 8/25/14.
 */
var Sequelize       = require('sequelize');
var config          = require('../config');
var sequelize = new Sequelize(config.dbName, config.sqlUser, config.sqlPassword);
var User = sequelize.define('User', {
  username:           Sequelize.STRING,
  password:           Sequelize.STRING,
  activated:          Sequelize.BOOLEAN,
  firstname:          Sequelize.STRING,
  lastname:           Sequelize.STRING
}, {
  tableName: 'User'
});

module.exports.User = User;