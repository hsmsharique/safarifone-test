/**
 * Created by hsmsharique on 8/25/14.
 */

var nodeMySql         = require('mysql');
var db                = require('../models/user');
var nodemailer        = require('nodemailer');
var config            = require('../config');
var transporter       = nodemailer.createTransport({
  service: config.emailService,
  auth: {
    user: config.emailUser,
    pass: config.emailUserPass
  }
});



module.exports = {


  /*createDatabase function is used to create database and further more to create sql table*/
  createDatabase : function(){
    var client = nodeMySql.createConnection({
      host: config.dbHost,
      user: config.sqlUser,
      password: config.sqlPassword
    });
    var ignore = [nodeMySql.ERROR_DB_CREATE_EXISTS,
      nodeMySql.ERROR_TABLE_EXISTS_ERROR];

    client.on('error', function (err) {
      if (ignore.indexOf(err.number) + 1) { return; }
      throw err;
    });
    client.query('CREATE DATABASE IF NOT EXISTS'+ config.dbName);
    client.query('USE '+ config.dbName);
    client.query(
        'CREATE TABLE User'+
        '(id INT(11) AUTO_INCREMENT, '+
        'username VARCHAR(255), '+
        'password VARCHAR(255), '+
        'firstname VARCHAR(255), '+
        'lastname VARCHAR(255), '+
        'activated BOOLEAN, '+
        'createdAt DATETIME, '+
        'updatedAt DATETIME, '+
        'PRIMARY KEY (id));', function(err, results) {
        if (err && err.number != client.ERROR_TABLE_EXISTS_ERROR) {
          console.log("ERROR: " + err.message);
          throw err;
        }
        console.log("TABLE User Created");
      }
    );
    client.end();
  },


  /*createUser is used to create user
  *
  *@args : data{ email,password,firstname,lastname}
  *
  * */

   createUser:function(data,callback){
    this.createDatabase();
    var localData = data;
    console.log('asd');
    db.User.find({where:{username:data.username}}).success(function(user){
      if(user !== null){
        callback(409,{ success:false, message:'This account already exists.'});
      }
      else{
        db.User.create(data).success(function() {
          var mailOptions = {
            from: 'Safarifone Email Service ✔ ',                        // sender address
            to: localData.firstname +','+localData.username,            // list of receivers
            subject: 'Account Activation ✔',                            // Subject line
            text: 'Hi '+localData.firstname+', Please visit this link to activate your account. https://localhost:5000/activate/user/'+localData.username // plaintext body
          };

          transporter.sendMail(mailOptions, function(error, info){
            if(error){
              console.log(error);
            }else{
              console.log('Message sent: ' + info.response);
              callback(200,{ success:true, message:'Account is created. Please visit the link sent to your provided email address.'});
            }
          });
        })
      }
    })

  },


  /*findUser is used to find user for login feature
   *
   *@args : data{ email,password}
   *
   * */

  findUser:function(data,callback){
    db.User.find({where:data}).success(function(user){
      if(user !== null){
        if(user.selectedValues.activated){
          callback(200,{success:true,message:'User authorized'});
        }
        else{
          callback(403,{success:false,message:'You are not activated yet. Please visit the link sent to your email address'});
        }
      }
      else{
        callback(403,{success:false,message:'User Unauthorized'});
      }
    })

  },


  /*drop is used to drop the database as per any requirements. [not necessary]
   *
   *@args : data{ req Object, res Object}
   *
   * */

  drop:function(req,res){
    var client = nodeMySql.createConnection({
      host: config.dbHost,
      user: config.sqlUser,
      password: config.sqlPassword
    });
    client.query(
        'DROP DATABASE '+ config.dbName, function(err, results) {
        if (err && err.number != client.ERROR_TABLE_EXISTS_ERROR) {
          console.log("ERROR: " + err.message);
          throw err;
        }
        console.log("Database Dropped");
        res.json({ success:true , message:'Database Dropped'});
      }
    );
    client.end();
  },

  /*activateUser is used to activate the user from the link sent to the user's provided email address
   *
   *@args : data{ userName, callBack Function}
   *
   * */


  activateUser:function(username,callback){
    db.User.find({where:{username:username}}).success(function(user){
      if(user !== null){
        if(user.selectedValues.activated){
          callback({success:true,message:'You are already activated'});
        }
        else{
          user.updateAttributes({
            activated:true
          }).success(function(user){
            callback({success:true,message:'You are now activated'});
          })
        }
      }
      else{
        callback({success:false,message:'This User does not exist'});
      }
    })
  }
};
